###################################
#  Nome: Jeorgivam Junior 	  	  #
#  Arquivo: app.py                #
#  Titulo: Desafio Python Evolux  #
###################################

#!/user/bin/env python
#-*- coding:utf-8 -*-

from flask import *
from flask.ext.sqlalchemy import SQLAlchemy 
import flask_wtf as wtf 
from wtforms import *
from werkzeug import secure_filename
import os

app = Flask(__name__)
app.config.from_object(__name__)
app.config.update({
	'SECRET_KEY': 'Evolux <3 Python',
	'DEBUG': True,
})

caminho = 'uploads'
foto = 'moes.jpg'
caminho_padrao = 'static/images'
app.config['UPLOAD_FOLDER'] = caminho
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///dados.sqlite3' 
db = SQLAlchemy(app) 

#classe das tabelas do banco
class Bar(db.Model): 
	__tablename__ = 'bares' 
	id = db.Column(db.Integer, primary_key = True, autoincrement=True) 
	nome = db.Column(db.String(100), nullable = False) 
	descricao = db.Column(db.Text, nullable = False)
	endereco = db.Column(db.Text, nullable = False)
	telefone = db.Column(db.String(7), nullable = False)
	especialidade = db.Column(db.Text, nullable = False)
	imagem = db.Column(db.Text, nullable = False)

#Cria o DB
db.metadata.create_all(db.engine, tables=[Bar.__table__])

mensagem1 = 'Campo Obrigatorio'
mensagem2 = 'No minimo e no maximo 6 caracteres'

#Estrutura do Formulario
class BarForm(wtf.Form):
	nome = TextField('Nome do Bar', [validators.Required(message=mensagem1)]) 
	descricao = TextAreaField('Descricao', [validators.Required(message=mensagem1)])
	imagem = FileField('Imagem')
 	endereco = TextField('Endereco', [validators.Required(message=mensagem1)])
 	telefone = TextField('Telefone', [validators.Required(message=mensagem1), validators.Length(min=6, max=6, message=mensagem2)])
 	especialidade = TextAreaField('Especialidade', [validators.Required(message=mensagem1)])

#Abre o index com o formulario para preencher
@app.route('/', methods=['GET', 'POST'])
def cadastro_bar():
	#recebe estrutura
	formulario = BarForm()
	#Quando for enviado via submit, grava os dados no banco
	if formulario.validate_on_submit():
		bar = Bar()
		bar.nome = formulario.nome.data
		bar.descricao = formulario.descricao.data
		if formulario.imagem.data:
			bar.imagem = secure_filename(formulario.imagem.data.filename)
			formulario.imagem.data.save(os.path.join(app.config['UPLOAD_FOLDER'], bar.imagem))
			#bar.imagem = os.path.join(app.config['UPLOAD_FOLDER'], bar.imagem)
		else:
			bar.imagem = foto
		bar.endereco = formulario.endereco.data
		bar.telefone = formulario.telefone.data
		bar.especialidade = formulario.especialidade.data
		db.session.add(bar)
		db.session.commit()
		return redirect(url_for("listar_bares"))
	#retorna o index
	return render_template("index.html", form=formulario)

@app.route('/uploads/<filename>')
def uploaded_file(filename):
	if filename == 'moes.jpg':
		return send_from_directory(caminho_padrao, filename)
	else:
		return send_from_directory(caminho, filename)
		
#lista todos os dados do banco
@app.route('/bares')
def listar_bares():
	bares = Bar.query.all()
	return render_template('listar.html', bares=bares)

#recebe a id via GET e apaga o dado do banco
@app.route('/apagar', methods=['POST'])
def apagar():	
	if request.method == 'POST':
		bar = Bar.query.filter_by(id=request.form['id']).first()
		db.session.delete(bar)
		db.session.commit()
		return redirect(url_for("listar_bares"))

#recebe a id via GET e edita o dado do banco
@app.route('/editar/<id>', methods=['GET', 'POST'])
def editar(id):
	bar = Bar.query.filter_by(id=id).first()
	formulario = BarForm()
	if formulario.validate_on_submit():
		if formulario.imagem.data:
			nova_imagem = secure_filename(formulario.imagem.data.filename)
			formulario.imagem.data.save(os.path.join(app.config['UPLOAD_FOLDER'], nova_imagem))
			#bar.imagem = os.path.join(app.config['UPLOAD_FOLDER'], bar.imagem)
		else:
			remove = request.form.get('remover_img', '')
			if remove != '':
				nova_imagem = foto
			else:
				nova_imagem = bar.imagem
			
		bar = Bar.query.filter_by(id=id).update(dict(nome = formulario.nome.data, descricao = formulario.descricao.data, imagem = nova_imagem, endereco = formulario.endereco.data, telefone = formulario.telefone.data, especialidade = formulario.especialidade.data))
		db.session.commit()
		return redirect(url_for("listar_bares"))
	return render_template("editar.html", form=formulario, bar=bar)
	
if __name__ == "__main__":
        app.run()

